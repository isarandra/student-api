package com.codewithisa.Student.API.repository;

import com.codewithisa.Student.API.entity.Student;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@Slf4j
@SpringBootTest
public class StudentRepositoryTest {
    @Autowired
    StudentRepository studentRepository;

    @Test
    void saveStudent(){
        Student student = Student
                .builder()
                .nama("Abdiyah")
                .nim(1706282349L)
                .jurusan("Biologi")
                .angkatan(2017)
                .build();
        try{
            studentRepository.save(student);
            log.info("student with name {} successfully added to the databse",student.getNama());
        }
        catch (Exception e){
            log.error("can't save student, student with name {} is already in database",student.getNama());
        }
    }

    @Test
    void findStudentsByJurusanAndAngkatan(){
        Optional<List<Student>> optionalStudentList = studentRepository.findStudentsByJurusanAndAngkatan(
                "Fisika",2017
        );
        if(!optionalStudentList.isPresent()){
            log.error("students not found");
        }
        List<Student> studentList = optionalStudentList.get();
        studentList.forEach(student -> {
            log.info(student.getNama());
        });
    }
}
