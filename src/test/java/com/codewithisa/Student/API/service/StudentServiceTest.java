package com.codewithisa.Student.API.service;

import com.codewithisa.Student.API.entity.Student;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@Slf4j
@SpringBootTest
public class StudentServiceTest {
    @Autowired
    StudentService studentService;

    @Test
    public void findStudentByNama(){
        try{
            Student student = studentService.findStudentByNama("Isa Randra");
        }
        catch (Exception e){
            log.error(e.getMessage());
        }
    }

    @Test
    void findStudentsByJurusanAndAngkatan(){
        try{
            List<Student> optionalStudentList = studentService.findStudentsByJurusanAndAngkatan("Fisika",2017);
        }
        catch (Exception e){
            log.error(e.getMessage());
        }
    }

    @Test
    void saveStudent(){
        Student student = Student
                .builder()
                .nama("Michael Jordan")
                .jurusan("Astronomi")
                .angkatan(2016)
                .nim(16984965L)
                .build();
        try {
            studentService.saveStudent(student);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
