package com.codewithisa.Student.API.repository;

import com.codewithisa.Student.API.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query(
            nativeQuery = true,
            value = "select * from student where nama = :nama"
    )
    Optional<Student> findStudentByNama(@Param("nama") String nama);

    @Query(
            nativeQuery = true,
            value = "select * from student where jurusan = :jurusan and angkatan = :angkatan"
    )
    Optional<List<Student>> findStudentsByJurusanAndAngkatan(
            @Param("jurusan") String jururan,
            @Param("angkatan") Integer angkatan
    );
}
