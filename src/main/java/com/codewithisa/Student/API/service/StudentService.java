package com.codewithisa.Student.API.service;

import com.codewithisa.Student.API.entity.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface StudentService {
    Student findStudentByNama(String nama) throws Exception;
    List<Student> findStudentsByJurusanAndAngkatan(String jurusan, Integer angkatan) throws Exception;
    Student saveStudent(Student student) throws Exception;
}
