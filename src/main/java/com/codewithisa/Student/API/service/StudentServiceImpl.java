package com.codewithisa.Student.API.service;

import com.codewithisa.Student.API.entity.Student;
import com.codewithisa.Student.API.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class StudentServiceImpl implements StudentService{

    @Autowired
    StudentRepository studentRepository;

    @Override
    public Student findStudentByNama(String nama) throws Exception{
        Optional<Student> studentOptional = studentRepository.findStudentByNama(nama);
        if(!studentOptional.isPresent()){
            log.error("student with name " + nama + " is not found");
            throw new Exception("student with name " + nama + " is not found");
        }
        Student student = studentOptional.get();
        log.info("student with name {} is found",student.getNama());
        return student;
    }

    @Override
    public List<Student> findStudentsByJurusanAndAngkatan(String jurusan, Integer angkatan) throws Exception {
        Optional<List<Student>> optionalStudentList = studentRepository.findStudentsByJurusanAndAngkatan(jurusan, angkatan);
        if(optionalStudentList.get().isEmpty()){
            log.error("students with angkatan = {} and jurusan = {} is not found",angkatan,jurusan);
            throw new Exception("students with angkatan = "+angkatan+" and jurusan = "+jurusan+" is not found");
        }
        List<Student> studentList = optionalStudentList.get();
        log.info("student(s) with angkatan = {} and jurusan = {} is found",angkatan,jurusan);
        return studentList;
    }

    @Override
    public Student saveStudent(Student student) throws Exception {
        try{
            studentRepository.save(student);
            log.info("student with name {} successfully added to the database",student.getNama());
            return student;
        }
        catch (Exception e){
            log.error("can't save student, student with name {} or nim {} is already in database",student.getNama(), student.getNim());
            throw new Exception("can't save student, student with name "+student.getNama()+" or nim " + student.getNim()+" is already in database");
        }
    }
}
