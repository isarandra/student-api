package com.codewithisa.Student.API.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public OpenAPI myApi(
            @Value(
                    "Student API digunakan untuk menyimpan data diri mahasiswa ke dalam database, " +
                            "mencari data diri mahasiswa berdasarkan nama serta mencari data diri seluruh " +
                            "mahasiswa yang berasal dari " +
                            "suatu jurusan dan angkatan." +
                            "\n\nUntuk informasi lebih lanjut silahkan hubungi " +
                            "isarandra@yahoo.com"
            ) String appDescription,
            @Value("v1.0.0") String appVersion) {
        return new OpenAPI()
                .info(new Info()
                        .title("Student API")
                        .version(appVersion)
                        .description(appDescription)
                        .termsOfService("http://swagger.io/terms")
                        .license(new License()
                                .name("Apache 2.0")
                                .url("http://springdoc.org")));
    }
}



