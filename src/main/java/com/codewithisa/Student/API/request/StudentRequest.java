package com.codewithisa.Student.API.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;

@Data
@Builder
public class StudentRequest {
    @Schema(example = "Isa Randra")
    private String nama;

    @Schema(example = "1706974725")
    private Long nim;

    @Schema(example = "2017")
    private Integer angkatan;

    @Schema(example = "Fisika")
    private String jurusan;
}
