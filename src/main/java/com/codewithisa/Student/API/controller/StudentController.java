package com.codewithisa.Student.API.controller;

import com.codewithisa.Student.API.entity.Student;
import com.codewithisa.Student.API.request.StudentRequest;
import com.codewithisa.Student.API.service.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentService studentService;

    @Operation(summary = "digunakan untuk mencari data diri mahasiswa berdasarkan nama")
    @GetMapping("/find-student-by-name")
    public ResponseEntity<StudentRequest> findStudentByName(@Schema(example = "Isa Randra") @RequestParam("nama") String nama){
        try {
            Student student = studentService.findStudentByNama(nama);
            log.info("student with name {} is found",nama);
            StudentRequest studentRequest = StudentRequest
                    .builder()
                    .nama(student.getNama())
                    .nim(student.getNim())
                    .jurusan(student.getJurusan())
                    .angkatan(student.getAngkatan())
                    .build();
            return new ResponseEntity<>(studentRequest, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Operation(summary = "digunakan untuk mencari data diri dari seluruh mahasiswa berdasarkan jurusan dan angkatan")
    @GetMapping("find-students-by-jurusan-and-angkatan")
    public ResponseEntity<List<Student>> findStudentsByJurusanAndAngkatan(
            @Schema(example = "Fisika") @RequestParam("jurusan") String jurusan,
            @Schema(example = "2017") @RequestParam("angkatan") Integer angkatan){
        try {
            List<Student> studentList = studentService.findStudentsByJurusanAndAngkatan(jurusan, angkatan);
            log.info("student(s) with angkatan = {} and jurusan = {} is found", angkatan, jurusan);
            return new ResponseEntity<>(studentList, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Operation(summary = "add a student to the database")
    @PostMapping("/add-student")
    public ResponseEntity<StudentRequest> addStudent(@RequestBody StudentRequest studentRequest){
        Student student = Student
                .builder()
                .nama(studentRequest.getNama())
                .nim(studentRequest.getNim())
                .angkatan(studentRequest.getAngkatan())
                .jurusan(studentRequest.getJurusan())
                .build();
        try {
            studentService.saveStudent(student);
            log.info("student with name {} is successfully added to the database");
            return new ResponseEntity<>(studentRequest, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
