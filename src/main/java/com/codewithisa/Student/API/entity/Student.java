package com.codewithisa.Student.API.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Student {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Schema(example = "1")
    private Long studentId;

    @Column(
            unique = true,
            nullable = false
    )
    @Schema(example = "Isa Randra")
    private String nama;

    @Column(
            unique = true,
            nullable = false
    )
    @Schema(example = "1706974725")
    private Long nim;

    @Column(
            nullable = false
    )
    @Schema(example = "2017")
    private Integer angkatan;

    @Column(
            nullable = false
    )
    @Schema(example = "Fisika")
    private String jurusan;
}
